#pragma once

template<class T>
class Mean {
public:
    Mean();

private:
    T _mean;
};

template<typename T>
Mean<T>::Mean() : _mean(0) {}