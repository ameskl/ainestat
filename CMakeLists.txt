cmake_minimum_required(VERSION 3.12)
project(AineStat)

set(CMAKE_CXX_STANDARD 17)

add_library(AineStat src/Mean.cpp include/Mean.h)